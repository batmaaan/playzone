package playzone.org.features.register

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import playzone.org.utils.isValidEmail

fun Application.configureRegisterRouting() {

  routing {
    post("/register") {
      val registerController = RegisterController(call)
      registerController.registerNewUser()


    }
  }
}
