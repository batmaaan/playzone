package playzone.org.features.register

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import org.jetbrains.exposed.exceptions.ExposedSQLException
import playzone.org.database.tokens.TokenDto
import playzone.org.database.tokens.Tokens
import playzone.org.database.users.UserDto
import playzone.org.database.users.Users
import playzone.org.utils.isValidEmail
import java.util.*

class RegisterController(private val call: ApplicationCall) {
  suspend fun registerNewUser() {
    val registerReceiveRemote = call.receive<RegisterReceiveRemote>()
    if (!registerReceiveRemote.email.isValidEmail()) {
      call.respond(HttpStatusCode.BadRequest, "Email is not Valid")
    }
    val userDto = Users.fetchUser(registerReceiveRemote.login)
    if (userDto != null) {
      call.respond(HttpStatusCode.Conflict, "User already exist")
    } else {
      val token = UUID.randomUUID().toString()
      try {
        Users.insert(
          UserDto(
            login = registerReceiveRemote.login,
            password = registerReceiveRemote.password,
            email = registerReceiveRemote.email,
            username = ""
          )
        )
      } catch (e: ExposedSQLException) {
        call.respond(HttpStatusCode.Conflict, "User already exist")
      }

      Tokens.insert(
        TokenDto(
          rowId = UUID.randomUUID().toString(),
          login = registerReceiveRemote.login,
          token = token
        )
      )
      call.respond(RegisterResponseRemote(token = token))

    }
  }
}
