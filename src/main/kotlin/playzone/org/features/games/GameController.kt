package playzone.org.features.games

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import playzone.org.database.games.Games
import playzone.org.database.games.mapToCreateGameResponse
import playzone.org.database.games.mapToGameDTO
import playzone.org.features.games.models.CreateGameRequest
import playzone.org.features.games.models.FetchGamesRequest
import playzone.org.utils.TokenCheck

class GameController(private val call: ApplicationCall) {

  suspend fun performSearch() {
    val request = call.receive<FetchGamesRequest>()
    val token = call.request.headers["Bearer-Authorization"]

    if (TokenCheck.isTokenValid(token.orEmpty()) || TokenCheck.isTokenAdmin(token.orEmpty())) {
      call.respond(Games.fetchAll().filter { it.title.contains(request.searchQuery, ignoreCase = true) })
    } else {
      call.respond(HttpStatusCode.Unauthorized, "Token expired")
    }
  }


  suspend fun createGame() {
    val token = call.request.headers["Bearer-Authorization"]
    if (TokenCheck.isTokenAdmin(token.orEmpty())) {
      val request = call.receive<CreateGameRequest>()
      val game = request.mapToGameDTO()
      Games.insert(game)
      call.respond(game.mapToCreateGameResponse())
    } else {
      call.respond(HttpStatusCode.Unauthorized, "Token expired")
    }
  }
}
