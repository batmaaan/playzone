package playzone.org.features.games

import io.ktor.server.application.*
import io.ktor.server.routing.*

fun Application.configureGamesRouting() {

  routing {
    post("/games/search") {

        val gamesController = GameController(call)
        gamesController.performSearch()

    }
    post("/games/create") {

        val gamesController = GameController(call)
        gamesController.createGame()

    }
  }
}


