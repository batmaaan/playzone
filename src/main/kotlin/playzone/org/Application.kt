package playzone.org

import io.ktor.server.cio.*
import io.ktor.server.engine.*
import org.jetbrains.exposed.sql.Database
import playzone.org.features.games.configureGamesRouting
import playzone.org.features.login.configureLoginRouting
import playzone.org.features.register.configureRegisterRouting
import playzone.org.plugins.configureRouting
import playzone.org.plugins.configureSerialization

fun main() {
  Database.connect(
    "jdbc:postgresql://localhost:5432/playzone", driver = "org.postgresql.Driver",
    user = "postgres", password = "1234"
  )
  embeddedServer(CIO, port = 8080, host = "0.0.0.0") {
    configureRouting()
    configureLoginRouting()
    configureRegisterRouting()
    configureSerialization()
    configureGamesRouting()
  }.start(wait = true)
}
