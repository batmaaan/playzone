create table users
(
    login    varchar(25) not null primary key,
    password varchar(25) not null,
    username varchar(30) not null,
    email    varchar

);
create table tokens
(
    id    varchar     not null primary key,
    login varchar(25) not null,
    token varchar(50) not null
);
create sequence games_id_seq;
create table games
(
    gameId varchar(100) default nextval('games'::regclass) not null,
    title varchar(100)  not null,
    backdrop varchar(50),
    logo varchar(50) not null ,
    description varchar(500) not null ,
    download_count INTEGER not null ,
    version varchar(15) not null ,
    size varchar(10) not null
);

alter table public.games
    add column "gameId" character varying(60) not null;
create table tournaments
(
    gameId varchar(100) primary key not null,
    name varchar(100)  not null,
    status INTEGER not null ,
    format varchar(20) not null ,
    prize varchar(25) not null ,
    watchers INTEGER not null
);

